﻿using DEQueueLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DEQueueTests
{
    class ValueTests
    {

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestIntVals([Range(0, 16)] int value)
        {
            DEQueue<int> q = new DEQueue<int>();
            List<int> refVals = new List<int>();
            Assert.AreEqual(q.Size, 0);
            Assert.Throws<ArgumentOutOfRangeException>(() => { var x = q.Back; });
            Assert.Throws<ArgumentOutOfRangeException>(() => { var x = q.Front; });

            for (int i = 0; i < value; ++i)
            {
                q.PushBack(i);
                refVals.Add(i);
                Assert.AreEqual(i, q.Back);
                Assert.AreEqual(0, q.Front);
                AreEqualArrays(refVals.ToArray(), q.ToArray());
            }

            q.Clear();
            refVals.Clear();
            for (int i = 0; i < value; ++i)
            {
                q.PushFront(i);
                refVals.Insert(0, i);
                Assert.AreEqual(0, q.Back);
                Assert.AreEqual(i, q.Front);
                AreEqualArrays(refVals.ToArray(), q.ToArray());
            }

            q.Clear();
            refVals.Clear();
            for (int i = 0; i < value; ++i)
            {
                if (i % 2 == 0)
                {
                    q.PushFront(i);
                    refVals.Insert(0, i);
                }
                else
                {
                    q.PushBack(i);
                    refVals.Add(i);
                }


                if (i == 0)
                {
                    Assert.AreEqual(0, q.Back);
                    Assert.AreEqual(0, q.Front);
                }
                else if (i % 2 == 0)
                {
                    Assert.AreEqual(i - 1, q.Back);
                    Assert.AreEqual(i, q.Front);
                }
                else
                {
                    Assert.AreEqual(i, q.Back);
                    Assert.AreEqual(i - 1, q.Front);
                }
                AreEqualArrays(refVals.ToArray(), q.ToArray());
            }

            q.Clear();
            refVals.Clear();
            for (int i = 0; i < value; ++i)
            {
                q.PushBack(i);
                refVals.Add(i);
                Assert.AreEqual(i, q.Back);
                Assert.AreEqual(i, q.Front);
                Assert.AreEqual(i, q.PopBack());
                refVals.RemoveAt(refVals.Count - 1);
            }
            AreEqualArrays(refVals.ToArray(), q.ToArray());

            q.Clear();
            refVals.Clear();
            for (int i = 0; i < value; ++i)
            {
                q.PushFront(i);
                refVals.Insert(0, i);
                if (i % 4 == 0)
                {
                    Assert.AreEqual(i, q.PopFront());
                    refVals.RemoveAt(0);
                }
                if (i % 4 == 2) {
                    var j = (i - 2) / 4;
                    Assert.AreEqual(j + 1 + j / 3, q.PopBack());
                    refVals.RemoveAt(refVals.Count - 1);
                }
                AreEqualArrays(refVals.ToArray(), q.ToArray());
            }

            q.Clear();
            refVals.Clear();
            for (int i = 0; i < value; ++i)
            {
                if (i % 3 != 0)
                {
                    q.PushFront(i);
                    refVals.Insert(0, i);
                }
                else
                {
                    q.PushBack(i);
                    refVals.Add(i);
                    Assert.AreEqual(i == 0 ? 0 : i - 1, q.PopFront());
                    refVals.RemoveAt(0);
                }
                Assert.AreEqual(2 * (i + 1) / 3, q.Size);
                AreEqualArrays(refVals.ToArray(), q.ToArray());
            }
        }

        private void AreEqualArrays<T>(T[] v1, T[] v2) where T : IComparable
        {
            Assert.AreEqual(v1.Length, v2.Length);
            for (int i = 0; i < v1.Length; ++i)
            {
                Assert.AreEqual(v1[i], v2[i]);
            }
        }
    }
}
