using NUnit.Framework;
using Banking;
using System;
using System.Collections.Generic;
using Moq;

namespace BankingTests
{
    public class WalletTests
    {
        private Mock<IBank> _bankMock;
        private Mock<IMoneyPrinter> _printerMock;

        public interface IMockPrinter
        {
            public string LastOp { get; set; }
        }

        [SetUp]
        public void Setup()
        {
            _bankMock = new Mock<IBank>();
            _bankMock.Setup(bank => bank.ConvertRate("USD", "RUB")).Returns(62.30);
            _bankMock.Setup(bank => bank.ConvertRate("JPY", "RUB")).Returns(0.57);
            _bankMock.Setup(bank => bank.ConvertRate("RUB", "RUB")).Returns(1.0);
            Func<double, string, string, double> valueFn = (amount, from, to) => _bankMock.Object.ConvertRate(from, to) * amount;
            _bankMock.Setup(bank => bank.Convert(
                It.IsAny<double>(), It.IsAny<string>(), It.IsAny<string>()
            )).Returns(valueFn);

            _printerMock = new Mock<IMoneyPrinter>();
            _printerMock.As<IMockPrinter>().SetupProperty(printer => printer.LastOp, "");
            Action<string, string, uint> action =
                (op, curr, amnt) => (_printerMock.Object as IMockPrinter).LastOp = $"{op} {amnt} {curr}";
            _printerMock.Setup(printer => printer.Print(
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<uint>()
            )).Callback(action);
        }

        [TestCase(100u, 50u)]
        [TestCase(100u, 100u)]
        [TestCase(50u, 100u)]
        public void TestTransfers(uint inAmnt, uint outAmnt)
        {
            Wallet w = new Wallet(_bankMock.Object, _printerMock.Object);

            foreach (string currency in new string[] { "RUB", "USD", "JPY" })
            {
                w.AddMoney(currency, inAmnt);
                Assert.AreEqual(w.GetMoney(currency), inAmnt);

                if (inAmnt >= outAmnt)
                {
                    w.RemoveMoney(currency, outAmnt);
                    Assert.AreEqual(w.GetMoney(currency), inAmnt - outAmnt);
                }
                else
                {
                    Assert.Throws<ArgumentOutOfRangeException>(() => w.RemoveMoney("RUB", outAmnt));
                }
            }
        }

        [Test]
        public void TestCurrencyCount(
            [Values(0, 1, 2)] int fullCurrs,
            [Values(0, 1, 2)] int withdrawnCurrs)
        {
            Wallet w = new Wallet(_bankMock.Object, _printerMock.Object);

            for (int curr = 0; curr < fullCurrs; curr++)
            {
                w.AddMoney(curr.ToString(), 1);
            }
            for (int curr = fullCurrs; curr < fullCurrs + withdrawnCurrs; curr++)
            {
                w.AddMoney(curr.ToString(), 1);
                w.RemoveMoney(curr.ToString(), 1);
            }

            Assert.AreEqual(w.CountCurrencies(), fullCurrs);
        }

        [Test]
        public void TestToString(
            [Values(0u, 100u, 200u)] uint jpy,
            [Values(0u, 100u, 200u)] uint rub,
            [Values(0u, 100u, 200u)] uint usd)
        {
            Wallet w = new Wallet(_bankMock.Object, _printerMock.Object);
            Assert.AreEqual("{ }", w.ToString());

            if (jpy == 0 && rub == 0 && usd == 0) return;

            w.AddMoney("JPY", jpy);
            w.AddMoney("RUB", rub);
            w.AddMoney("USD", usd);

            List<string> refTokens = new List<string>();
            if (jpy != 0)
                refTokens.Add($"{jpy} JPY");
            if (rub != 0)
                refTokens.Add($"{rub} RUB");
            if (usd != 0)
                refTokens.Add($"{usd} USD");
            string refStr = string.Join(", ", refTokens);
            refStr = $"{{ {refStr} }}";

            Assert.AreEqual(refStr, w.ToString());
            Console.WriteLine(refStr);
        }

        [Test]
        public void TestTotalMoney(
            [Values(0u, 100u, 200u)] uint jpy,
            [Values(0u, 100u, 200u)] uint rub,
            [Values(0u, 100u, 200u)] uint usd)
        {
            uint target = Convert.ToUInt32(
                Math.Round(1.0 * rub + 62.30 * usd + 0.57 * jpy));

            Wallet w = new Wallet(_bankMock.Object, _printerMock.Object);
            w.AddMoney("JPY", jpy);
            w.AddMoney("RUB", rub);
            w.AddMoney("USD", usd);

            Assert.AreEqual(target, w.TotalMoney("RUB"));
        }

        [Test]
        public void TestPrinting()
        {
            Wallet w = new Wallet(_bankMock.Object, _printerMock.Object);
            w.AddMoney("RUB", 100);
            Assert.AreEqual((_printerMock.Object as IMockPrinter).LastOp, "+ 100 RUB");

            w.RemoveMoney("RUB", 75);
            Assert.AreEqual((_printerMock.Object as IMockPrinter).LastOp, "- 75 RUB");
        }
    }
}