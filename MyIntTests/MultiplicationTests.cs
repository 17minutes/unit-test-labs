﻿using MyIntLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace MyIntTests
{
    class MultiplicationTests
    {
        private Random _rnd;

        [SetUp]
        public void Setup()
        {
            _rnd = new Random(31337);
        }

        private void CheckAssoc(MyInt a, MyInt b)
        {
            var lhs = a.Multiply(b);
            var rhs = b.Multiply(a);
            Assert.Zero(lhs.CompareTo(rhs));
        }

        private void CheckZero(MyInt a)
        {
            var lhs = MyInt.ZERO;
            var rhs = a.Multiply(MyInt.ZERO);
            Assert.Zero(lhs.CompareTo(rhs));
        }

        [Test, Pairwise]
        public void TestMultiplication([Range(4, 128, 4)] int digitsLhs, [Range(0, 64, 8)] int extraDigits, [Values(0, 1)] byte signLhs, [Values(0, 1)]byte signRhs)
        {
            var digitsRhs = digitsLhs + extraDigits;

            for (int trial = 1; trial <= 32; ++trial)
            {
                byte[] lhsBytes = new byte[digitsLhs + 1];
                lhsBytes[0] = signLhs;
                byte[] rhsBytes = new byte[digitsRhs + 1];
                rhsBytes[0] = signRhs;

                for (int i = 1; i <= digitsLhs; ++i)
                {
                    lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }
                for (int i = 1; i <= digitsRhs; ++i)
                {
                    rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsBytes);

                var refStr = (BigInteger.Parse(lhs.ToString()) * BigInteger.Parse(rhs.ToString())).ToString();
                Assert.AreEqual(
                    refStr, lhs.Multiply(rhs).ToString(),
                    $"{lhs} * {rhs} = {refStr} but Multiply gives {lhs.Multiply(rhs)} @ trial {trial}");

                CheckAssoc(lhs, rhs);
                CheckZero(lhs);
            }
        }
    }
}
