﻿using MyIntLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyIntTests
{
    class SubtractionTests
    {
        private Random _rnd;

        [SetUp]
        public void Setup()
        {
            _rnd = new Random(31337);
        }

        private void CheckPredicate(MyInt a, MyInt b)
        {
            var sub = a.Subtract(b);
            Assert.Zero(sub.Add(b).CompareTo(a),
                $"({a} - {b}) + {b} != {sub.Add(b)})");
        }

        [Test, Pairwise]
        public void TestProperty([Range(4, 128, 4)] int digitsLhs, [Range(0, 64, 8)] int extraDigits, [Values(0, 1)] byte signLhs, [Values(0, 1)]byte signRhs)
        {
            var digitsRhs = digitsLhs + extraDigits;

            for (int trial = 1; trial <= 32; ++trial)
            {
                byte[] lhsBytes = new byte[digitsLhs + 1];
                lhsBytes[0] = signLhs;
                byte[] rhsBytes = new byte[digitsRhs + 1];
                rhsBytes[0] = signRhs;

                for (int i = 1; i <= digitsLhs; ++i)
                {
                    lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }
                for (int i = 1; i <= digitsRhs; ++i)
                {
                    rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsBytes);

                CheckPredicate(lhs, rhs);
            }
        }
    }
}
