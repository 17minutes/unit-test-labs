﻿using MyIntLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyIntTests
{
    class AbsTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(100)]
        [TestCase(1024)]
        [TestCase(65536)]
        [TestCase(100000)]
        [TestCase(1L << 31)]
        [TestCase(1L << 32)]
        [TestCase((1L << 32) + (1L << 31))]
        [TestCase(long.MaxValue)]
        public void TestShortInt(long val)
        {
            var target = val.ToString();

            var pos = new MyInt(+val);
            var neg = new MyInt(-val);

            Assert.AreEqual(target, pos.Abs().ToString());
            Assert.AreEqual(target, neg.Abs().ToString());

            Assert.Zero(pos.CompareTo(pos.Abs()));
            var negAbs = neg.Abs();
            neg.Sign = true;
            Assert.Zero(neg.CompareTo(negAbs), $"{neg} != {negAbs}");
        }

        [TestCase("100000000000000000")]
        [TestCase("1000000000000000000")]
        [TestCase("9223372036854775807")]
        [TestCase("100000000000000000000000")]
        [TestCase("123456789123456000000000")]
        [TestCase("92233720368547758079223372036854775807")]
        [TestCase("922337203685477580700000000000000000000009223372036854775807")]
        public void TestBigNumbers(string val)
        {
            for (int leadingZeros = 0; leadingZeros <= 20; leadingZeros++)
            {
                var posVal = val;
                for (int i = 0; i < leadingZeros; ++i) posVal = "0" + posVal;
                var negVal = $"-{posVal}";

                var pos = new MyInt(val);
                var neg = new MyInt(negVal);

                Assert.AreEqual(
                    val, pos.Abs().ToString(),
                    $"Leading zeros used: {leadingZeros}");
                Assert.AreEqual(
                    val, neg.Abs().ToString(),
                    $"Leading zeros used: {leadingZeros}");

                Assert.Zero(pos.CompareTo(pos.Abs()));
                var negAbs = neg.Abs();
                neg.Sign = true;
                Assert.Zero(neg.CompareTo(negAbs));
            }
        }
    }
}
