﻿using MyIntLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyIntTests
{
    class CompareTests
    {
        private Random _rnd;

        [SetUp]
        public void Setup()
        {
            _rnd = new Random(31337);
        }

        [Test]
        public void TestOppositeSigns([Range(64, 128, 8)] int digits)
        {
            for (int trial = 1; trial <= 32; ++trial)
            {
                byte[] lhsBytes = new byte[digits + 1];
                lhsBytes[0] = 1;
                byte[] rhsBytes = new byte[digits + 1];
                rhsBytes[0] = 0;

                for (int i = 1; i <= digits; ++i)
                {
                    lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                    rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsBytes);
                Assert.Negative(lhs.CompareTo(rhs), $"{lhs} < {rhs}");
                Assert.Positive(rhs.CompareTo(lhs), $"{rhs} > {lhs}");
            }
        }

        [Test]
        public void TestVariousLength([Range(16, 128, 8)] int digitsLhs, [Range(8, 64, 8)] int extraDigits)
        {
            var digitsRhs = digitsLhs + extraDigits;

            for (int trial = 1; trial <= 8; ++trial)
            {
                byte[] lhsBytes = new byte[digitsLhs + 1];
                lhsBytes[0] = 0;
                byte[] rhsBytes = new byte[digitsRhs + 1];
                rhsBytes[0] = 0;

                for (int i = 1; i <= digitsLhs; ++i)
                {
                    lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }
                for (int i = 1; i <= digitsRhs; ++i)
                {
                    rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsBytes);
                Assert.Negative(lhs.CompareTo(rhs), $"{lhs} < {rhs} has failed");
                Assert.Positive(rhs.CompareTo(lhs), $"{rhs} > {lhs} has failed");

                lhs.Sign = false;
                rhs.Sign = false;
                Assert.Positive(lhs.CompareTo(rhs), $"{lhs} > {rhs} has failed");
                Assert.Negative(rhs.CompareTo(lhs), $"{rhs} < {lhs} has failed");
            }
        }

        [Test]
        public void TestSameLength([Range(128, 256, 8)] int digits)
        {
            for (int trial = 1; trial <= 4 * digits; ++trial)
            {
                var switchPoint = _rnd.Next(1, digits + 1);

                byte[] lhsBytes = new byte[digits + 1];
                lhsBytes[0] = 0;
                byte[] rhsBytes = new byte[digits + 1];
                rhsBytes[0] = 0;

                for (int i = 1; i <= digits; ++i)
                {
                    lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                    if (i < switchPoint)
                        rhsBytes[i] = lhsBytes[i];
                    else if (i == switchPoint)
                    {
                        lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 9));
                        rhsBytes[i] = Convert.ToByte(_rnd.Next(lhsBytes[i] + 1, 10));
                    }
                    else
                        rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsBytes);
                Assert.Negative(lhs.CompareTo(rhs), $"{lhs} < {rhs} has failed");
                Assert.Positive(rhs.CompareTo(lhs), $"{rhs} > {lhs} has failed");

                lhs.Sign = false;
                rhs.Sign = false;
                Assert.Positive(lhs.CompareTo(rhs), $"{lhs} > {rhs} has failed");
                Assert.Negative(rhs.CompareTo(lhs), $"{rhs} < {lhs} has failed");
            }
        }

        [Test]
        public void TestEquality([Range(128, 256, 8)] int digits)
        {
            for (int trial = 1; trial <= 48; ++trial)
            {
                byte[] lhsBytes = new byte[digits + 1];
                lhsBytes[0] = 0;
                string rhsString = "";

                for (int i = 1; i <= digits; ++i)
                {
                    var d = _rnd.Next(0, 10);
                    lhsBytes[i] = Convert.ToByte(d);
                    rhsString += d.ToString();
                }

                var lhs = new MyInt(lhsBytes);
                var rhs = new MyInt(rhsString);
                Assert.Zero(lhs.CompareTo(rhs), $"{lhs} != {rhs}");
                Assert.Zero(rhs.CompareTo(lhs), $"{rhs} != {lhs}");

                lhs.Sign = false;
                rhs.Sign = false;
                Assert.Zero(lhs.CompareTo(rhs), $"{lhs} != {rhs}");
                Assert.Zero(rhs.CompareTo(lhs), $"{rhs} != {lhs}");

            }
        }
    }
}
