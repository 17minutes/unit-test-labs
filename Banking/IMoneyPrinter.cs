﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking
{
    public interface IMoneyPrinter
    {
        public void Print(string operation, string currency, uint amount);
    }
}
