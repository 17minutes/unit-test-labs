using NUnit.Framework;
using MyIntLib;
using System;

namespace MyIntTests
{
    public class InitTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(100)]
        [TestCase(1024)]
        [TestCase(65536)]
        [TestCase(100000)]
        [TestCase(1L << 31)]
        [TestCase(1L << 32)]
        [TestCase((1L << 32) + (1L << 31))]
        [TestCase(long.MaxValue)]
        public void TestShortInt(long val)
        {
            Assert.AreEqual((+val).ToString(), new MyInt(+val).ToString());
            Assert.AreEqual((-val).ToString(), new MyInt(-val).ToString());

            Assert.AreEqual((+val).ToString(), new MyInt((+val).ToString()).ToString());
            Assert.AreEqual((-val).ToString(), new MyInt((-val).ToString()).ToString());

            for (byte sign = 0; sign <= 1; ++sign)
            {
                byte[] bytes = new byte[20];
                bytes[0] = sign;
                var tmp = val;
                for (int i = 19; i > 0; i--)
                {
                    bytes[i] = Convert.ToByte(tmp % 10);
                    tmp /= 10;
                }
                var target = sign == 1 ? -val : +val;
                Assert.AreEqual(target.ToString(), new MyInt(bytes).ToString());
            }
        }

        [TestCase("100000000000000000")]
        [TestCase("1000000000000000000")]
        [TestCase("9223372036854775807")]
        [TestCase("100000000000000000000000")]
        [TestCase("123456789123456000000000")]
        [TestCase("92233720368547758079223372036854775807")]
        [TestCase("922337203685477580700000000000000000000009223372036854775807")]
        public void TestStringRepr(string val)
        {
            for (int leadingZeros = 0; leadingZeros <= 20; leadingZeros++)
            {
                var posVal = val;
                for (int i = 0; i < leadingZeros; ++i) posVal = "0" + posVal;
                var negVal = $"-{posVal}";
                Assert.AreEqual(
                    val, new MyInt(val).ToString(),
                    $"Leading zeros used: {leadingZeros}");
                Assert.AreEqual(
                    $"-{val}", new MyInt(negVal).ToString(),
                    $"Leading zeros used: {leadingZeros}");
            }
        }

        [TestCase(new byte[] { 0, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 })]
        public void TestByteArrayRepr(byte[] val)
        {
            for (byte sign = 0; sign <= 1; ++sign)
            {
                val[0] = sign;
                string repr = "";
                if (sign == 1) repr = "-";
                for (int i = 1; i < val.Length; ++i)
                    repr += val[i].ToString();
                Assert.AreEqual(
                    repr, new MyInt(val).ToString());
            }
        }
    }
}