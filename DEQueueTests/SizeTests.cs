using NUnit.Framework;
using DEQueueLib;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DEQueueTests
{
    public class SizeTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestValidOps([Range(0, 16)] int value)
        {
            DEQueue<int> q = new DEQueue<int>();
            for (int i = 0; i < value; ++i)
            {
                q.PushBack(i);
                Assert.AreEqual(1, q.Size);
                q.PopBack();
                Assert.AreEqual(0, q.Size);
            }

            q.Clear();
            Assert.AreEqual(q.Size, 0);
            for (int i = 0; i < value; ++i)
            {
                q.PushFront(i);
                if (i % 4 == 0) q.PopFront();
                if (i % 4 == 2) q.PopBack();
                Assert.AreEqual((i + 1) / 2, q.Size);
            }

            q.Clear();
            Assert.AreEqual(q.Size, 0);
            for (int i = 0; i < value; ++i)
            {
                if (i % 3 != 0)
                    q.PushFront(i);
                else
                {
                    q.PushBack(i);
                    q.PopFront();
                }
                Assert.AreEqual(2 * (i + 1) / 3, q.Size);
            }
        }

        [Test]
        public void TestInvalidOps([Range(0, 16)] int value)
        {
            DEQueue<int> q = new DEQueue<int>();
            for (int i = 0; i < value; ++i)
            {
                q.PushBack(i);
                Assert.AreEqual(1, q.Size);
                q.PopBack();
                Assert.AreEqual(0, q.Size);
                Assert.Throws<ArgumentOutOfRangeException>(() => q.PopFront());
                Assert.Throws<ArgumentOutOfRangeException>(() => q.PopBack());
            }
        }


        [Test]
        public void TestFillOnly([Range(0, 16)] int value)
        {
            DEQueue<int> q = new DEQueue<int>();
            Assert.AreEqual(q.Size, 0);
            for (int i = 0; i < value; ++i)
            {
                q.PushBack(i);
                Assert.AreEqual(i + 1, q.Size);
            }

            q.Clear();
            Assert.AreEqual(q.Size, 0);
            for (int i = 0; i < value; ++i)
            {
                q.PushFront(i);
                Assert.AreEqual(i + 1, q.Size);
            }

            q.Clear();
            Assert.AreEqual(q.Size, 0);
            for (int i = 0; i < value; ++i)
            {
                if (i % 2 == 0)
                    q.PushFront(i);
                else
                    q.PushBack(i);
                Assert.AreEqual(i + 1, q.Size);
            }
        }
    }
}