﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DEQueueLib
{
    public class DEQueue<T> : IEnumerable<T>
    {
        private T[] _buffer;
        private int _capacity => _buffer.Length;

        private int _left, _right;

        public DEQueue()
        {
            _buffer = new T[] { default };
            _left = _right = 0;
        }

        public int Size => _right - _left;

        private void Extend()
        {
            var _newBuf = new T[2 * _capacity];
            int cnt = 0;
            foreach (T el in this)
            {
                _newBuf[cnt++] = el;
            }
            _buffer = _newBuf;
            _left = 0;
            _right = cnt;
        }

        public void PushBack(T elem)
        {
            if (Size == _capacity) Extend();
            var ix = _right < _capacity ? _right : _right - _capacity;
            _buffer[ix] = elem;
            _right++;
        }

        public void PushFront(T elem)
        {
            if (Size == _capacity) Extend();
            _left--;
            var ix = _left >= 0 ? _left : _left + _capacity;
            _buffer[ix] = elem;
        }

        public T PopBack()
        {
            var res = Back;
            _right--;
            return res;
        }

        public T PopFront()
        {
            var res = Front;
            _left++;
            return res;
        }

        public T Back
        {
            get
            {
                if (Size == 0)
                    throw new ArgumentOutOfRangeException();
                return _buffer[(_right + _capacity - 1) % _capacity];
            }
        }

        public T Front
        {
            get
            {
                if (Size == 0)
                    throw new ArgumentOutOfRangeException();
                return _buffer[(_left + _capacity) % _capacity];
            }
        }

        public void Clear()
        {
            _buffer = new T[] { default };
            _left = _right = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Size; ++i)
            {
                yield return _buffer[(_left + _capacity + i) % _capacity];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
