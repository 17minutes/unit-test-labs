﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking
{
    public interface IBank
    {
        public double Convert(double amount, string from, string to) => System.Convert.ToUInt32(
            Math.Round(amount * ConvertRate(from, to)));

        public double ConvertRate(string from, string to);
    }
}
