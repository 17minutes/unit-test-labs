﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyIntLib
{
    public class MyInt : IComparable<MyInt>
    {
        private List<ulong> _digits;
        public bool Sign { get; set; }
        public bool IsNegative => !IsNonnegative;
        public bool IsNonnegative => Sign || IsZero;
        public bool IsZero => _digits.Count == 1 && _digits[0] == 0;

        public static MyInt ZERO = new MyInt(0);

        public const ulong BASE = 1_000_000_000;

        public const int BASE_LEN = 9;

        public MyInt(MyInt o)
        {
            Sign = o.Sign;
            _digits = o._digits.Select(x => x).ToList();
        }

        public MyInt(long val = 0)
        {
            Sign = val >= 0;
            _digits = new List<ulong> { Convert.ToUInt64(Math.Abs(val)) };
        }

        public MyInt(string val)
        {
            if (val.Length == 0)
                throw new ArgumentOutOfRangeException();
            if (val[0] == '-') {
                Sign = false;
                val = val.Substring(1);
            }
            else { Sign = true; }

            if (val.Length < BASE_LEN)
            {
                _digits = new List<ulong> { Convert.ToUInt64(val) };
            }
            else
            {
                _digits = new List<ulong>();
                for (int i = BASE_LEN; i <= val.Length + BASE_LEN; i += BASE_LEN)
                {
                    ulong next;
                    var substrStart = val.Length - i;
                    var len = BASE_LEN;
                    if (substrStart < 0)
                    {
                        len += substrStart;
                        substrStart = 0;
                    }
                    if (len == 0) break;
                    if (!ulong.TryParse(
                        val.Substring(substrStart, len),
                        out next))
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    _digits.Add(next);
                }
            }

            Normalize();
        }

        public MyInt Abs()
        {
            MyInt res = new MyInt(this);
            if (res.IsNegative)
                res.Sign = true;
            return res;
        }

        public MyInt(byte[] val)
        {
            if (val.Length == 0)
                throw new ArgumentOutOfRangeException();
            if (val[0] != 0 && val[0] != 1)
                throw new ArgumentOutOfRangeException();
            Sign = val[0] == 0;
            _digits = new List<ulong>();
            ulong mul = 1;
            for (int i = 1; i <= val.Length - 1; ++i)
            {
                if ((i - 1) % BASE_LEN == 0)
                {
                    _digits.Add(0);
                    mul = 1;
                }
                _digits[_digits.Count - 1] += mul * val[val.Length - i];
                mul *= 10;
            }

            Normalize();
        }

        private void Normalize()
        {
            while (_digits.Count > 1 && _digits[_digits.Count - 1] == 0)
                _digits.RemoveAt(_digits.Count - 1);
            if (IsZero)
                Sign = true;
        }

        public override string ToString()
        {
            string res = "";
            if (IsNegative) res += "-";
            for (int i = _digits.Count - 1; i >= 0; i--)
            {
                string newdigit = _digits[i].ToString();
                if (i < _digits.Count - 1)
                {
                    while (newdigit.Length < BASE_LEN)
                        newdigit = "0" + newdigit;
                }
                res += newdigit;
            }
            return res;
        }

        public long ToLong() => Convert.ToInt64(ToString());

        public int CompareTo(MyInt other)
        {
            if (IsNegative && other.IsNonnegative)
                return -1;
            else if (IsNonnegative && other.IsNegative)
                return +1;
            else if (IsNegative && other.IsNegative)
                return -Abs().CompareTo(other.Abs());
            else
            {
                if (_digits.Count < other._digits.Count)
                    return -1;
                else if (_digits.Count > other._digits.Count)
                    return +1;
                else
                {
                    for (int i = _digits.Count - 1; i >= 0; --i)
                    {
                        if (_digits[i] < other._digits[i])
                            return -1;
                        else if (_digits[i] > other._digits[i])
                            return +1;
                        else continue;
                    }
                    return 0;
                }
            }
        }

        public MyInt Min(MyInt b)
        {
            return CompareTo(b) < 0 ? this : b;
        }

        public MyInt Max(MyInt b)
        {
            return CompareTo(b) < 0 ? b : this;
        }

        public MyInt Add(MyInt o)
        {
            if (Sign == o.Sign)
            {
                MyInt res = new MyInt();
                ulong carry = 0;
                var newDigits = new List<ulong>();
                for (int i = 0; i < _digits.Count || i < o._digits.Count; ++i)
                {
                    ulong a = this._digits.ElementAtOrDefault(i);
                    ulong b = o._digits.ElementAtOrDefault(i);
                    newDigits.Add((a + b + carry) % BASE);
                    carry = (a + b + carry) / BASE;
                }
                newDigits.Add(carry);
                res._digits = newDigits;
                res.Sign = Sign;
                res.Normalize();
                return res;
            }
            else
            {
                if (!Sign) return o.Add(this);
                else return Subtract(o.Abs());
            }
        }

        public MyInt Subtract(MyInt o)
        {
            if (Sign == o.Sign)
            {
                if (!Sign) return o.Abs().Subtract(Abs());
                else
                {
                    if (CompareTo(o) < 0)
                    {
                        var res = o.Subtract(this);
                        res.Sign = false;
                        return res;
                    }
                    else
                    {
                        MyInt res = new MyInt();
                        ulong carry = 0;
                        var newDigits = new List<ulong>();
                        for (int i = 0; i < _digits.Count || i < o._digits.Count; ++i)
                        {
                            ulong a = this._digits.ElementAtOrDefault(i);
                            ulong b = o._digits.ElementAtOrDefault(i);
                            if (a >= b + carry)
                            {
                                newDigits.Add(a - b - carry);
                                carry = 0;
                            }
                            else
                            {
                                newDigits.Add(BASE + a - b - carry);
                                carry = 1;
                            }
                        }
                        newDigits.Add(carry);
                        res._digits = newDigits;
                        res.Sign = Sign;
                        res.Normalize();
                        return res;
                    }
                }
            }
            else
            {
                if (!Sign)
                {
                    var res = o.Subtract(this);
                    res.Sign = !res.Sign;
                    return res;
                }
                else
                {
                    return Add(o.Abs());
                }
            }
        }

        public MyInt Multiply(MyInt o)
        {
            if (!(Sign && o.Sign))
            {
                var res = Abs().Multiply(o.Abs());
                var sign = Sign == o.Sign;
                res.Sign = sign;
                return res;
            }
            else
            {
                MyInt res = new MyInt();
                var newDigits = Enumerable.Repeat(0UL, _digits.Count + o._digits.Count).ToList();
                for (int i = 0; i < o._digits.Count; ++i)
                {
                    ulong carry = 0;
                    for (int j = 0; j < _digits.Count; ++j)
                    {
                        newDigits[i + j] += carry + _digits[j] * o._digits[i];
                        carry = newDigits[i + j] / BASE;
                        newDigits[i + j] %= BASE;
                    }
                    newDigits[i + _digits.Count] = carry;
                }
                res._digits = newDigits;
                res.Normalize();
                return res;
            }
        }
    }
}
