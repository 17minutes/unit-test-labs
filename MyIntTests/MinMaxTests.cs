﻿using MyIntLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyIntTests
{
    class MinMaxTests
    {
        private Random _rnd;

        [SetUp]
        public void Setup()
        {
            _rnd = new Random(31337);
        }

        public void Predicate(MyInt a, MyInt b)
        {
            MyInt lb = a.Min(b), ub = a.Max(b);

            Assert.IsTrue(lb.CompareTo(a) == 0 || lb.CompareTo(b) == 0);
            Assert.IsTrue(ub.CompareTo(a) == 0 || ub.CompareTo(b) == 0);
            
            Assert.GreaterOrEqual(ub.CompareTo(a), 0);
            Assert.GreaterOrEqual(ub.CompareTo(b), 0);

            Assert.LessOrEqual(lb.CompareTo(a), 0);
            Assert.LessOrEqual(lb.CompareTo(b), 0);

            Assert.Zero(lb.CompareTo(b.Min(a)));
            Assert.Zero(ub.CompareTo(b.Max(a)));
        }

        [Test]
        public void TestProperty([Range(16, 128, 8)] int digitsLhs, [Range(0, 64, 8)] int extraDigits)
        {
            var digitsRhs = digitsLhs + extraDigits;

            for (byte signLhs = 0; signLhs <= 1; ++signLhs)
            {
                for (byte signRhs = 0; signRhs <= 1; ++signRhs)
                {
                    for (int trial = 1; trial <= 8; ++trial)
                    {
                        byte[] lhsBytes = new byte[digitsLhs + 1];
                        lhsBytes[0] = signLhs;
                        byte[] rhsBytes = new byte[digitsRhs + 1];
                        rhsBytes[0] = signRhs;

                        for (int i = 1; i <= digitsLhs; ++i)
                        {
                            lhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                        }
                        for (int i = 1; i <= digitsRhs; ++i)
                        {
                            rhsBytes[i] = Convert.ToByte(_rnd.Next(0, 10));
                        }

                        var lhs = new MyInt(lhsBytes);
                        var rhs = new MyInt(rhsBytes);

                        Predicate(lhs, rhs);
                    }
                }
            }
        }
    }
}
