﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Banking
{
    public class Wallet : IEnumerable<KeyValuePair<string, uint>>
    {
        private Dictionary<string, uint> _walletContents;
        private IBank _bank;
        private IMoneyPrinter _printer;

        public Wallet(IBank bank, IMoneyPrinter printer) {
            _walletContents = new Dictionary<string, uint>();
            _bank = bank;
            _printer = printer;
        }

        public void AddMoney(string currency, uint inAmnt)
        {
            if (!_walletContents.ContainsKey(currency))
                _walletContents[currency] = 0;
            _walletContents[currency] += inAmnt;
            _printer.Print("+", currency, inAmnt);
        }

        public void RemoveMoney(string currency, uint outAmnt)
        {
            uint bal = 0;
            if (_walletContents.ContainsKey(currency))
                bal = _walletContents[currency];
            if (bal < outAmnt)
                throw new ArgumentOutOfRangeException();
            _walletContents[currency] -= outAmnt;
            _printer.Print("-", currency, outAmnt);
        }

        public uint GetMoney(string currency)
        {
            return _walletContents.GetValueOrDefault(currency);
        }

        public double CountCurrencies() => this.Count();

        IEnumerator<KeyValuePair<string, uint>> IEnumerable<KeyValuePair<string, uint>>.GetEnumerator()
        {
            return _walletContents
                .Where(kv => kv.Value != 0)
                .OrderBy(kv => kv.Key)
                .GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public override string ToString()
        {
            string currs = string.Join(", ", this.Select(kv => $"{kv.Value} {kv.Key}"));
            return currs != "" ? $"{{ {currs} }}" : "{ }";
        }

        public uint TotalMoney(string currency)
        {
            double res = 0.0;
            foreach (var x in this.Select(kv => _bank.Convert(kv.Value, kv.Key, currency)))
                res += x;
            return Convert.ToUInt32(Math.Round(res));
        }
    }
}
